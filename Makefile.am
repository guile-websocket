# guile-websocket --- WebSocket client/server
# Copyright © 2015 David Thompson <davet@gnu.org>
# Copyright © 2020 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of guile-websocket.
#
# Guile-websocket is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Guile-websocket is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with guile-websocket.  If not, see
# <http://www.gnu.org/licenses/>.

GOBJECTS = $(SOURCES:%.scm=%.go)

nobase_mod_DATA = $(SOURCES) $(NOCOMP_SOURCES)
nobase_go_DATA = $(GOBJECTS)

# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guile_install_go_files = install-nobase_goDATA
$(guile_install_go_files): install-nobase_modDATA

CLEANFILES = $(GOBJECTS)
EXTRA_DIST = $(SOURCES) $(NOCOMP_SOURCES)
GUILE_WARNINGS = -Wunbound-variable -Warity-mismatch -Wformat
SUFFIXES = .scm .go
.scm.go:
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE_TOOLS) compile --target="$(host)" $(GUILE_WARNINGS) -o "$@" "$<"

moddir=$(datadir)/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

SOURCES =					\
  web/socket/client.scm				\
  web/socket/frame.scm				\
  web/socket/sha-1.scm				\
  web/socket/utils.scm				\
  web/socket/server.scm

TESTS =						\
  tests/frame.scm

CLEANFILES +=						\
  $(TESTS:tests/%.scm=%.log)

TEST_EXTENSIONS = .scm
SCM_LOG_COMPILER = $(top_builddir)/pre-inst-env $(GUILE)
AM_SCM_LOG_FLAGS = --no-auto-compile -L "$(top_srcdir)"

EXTRA_DIST +=					\
  COPYING					\
  pre-inst-env.in				\
  README					\
  guix.scm					\
  examples/echo.scm				\
  test.html					\
  test.scm					\
  $(TESTS)

publish: distcheck
	gpg --sign --detach-sign --armor --yes guile-websocket-$(VERSION).tar.gz && \
        scp guile-websocket-$(VERSION).tar.gz guile-websocket-$(VERSION).tar.gz.asc \
	    publish@dthompson.us:/var/www/files/guile-websocket/
