(use-modules (ice-9 match)
             (web socket server)
             (web socket client))

;; Respond to text messages by reversing the message.  Respond to
;; binary messages with "hello".
(define (handle-client ws)
  (unless (port-closed? ws)
    (match (websocket-receive ws #:echo-close? #t)
      ((? string? str)
       (pk 'string-message str)
       (websocket-send ws (string-reverse str) #:mask? #f))
      (bv
       (pk 'binary-message bv)
       (websocket-send ws "hello" #:mask? #f)))
    (handle-client ws)))

(run-websocket-server handle-client
                      #:server-socket (make-server-socket #:port 9090))
